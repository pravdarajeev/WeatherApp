/**
 * 
 */
package main.com.weatherapp.dto;

import java.util.Calendar;

import main.com.weatherapp.enums.Climate;

/**
 * @author abhilash
 *
 */
public class WeatherDTO {
	// Sydney|-33.86,151.21,39|2015-12-23T05:02:12Z|Rain|+12.5|1004.3|97
	// Location Position Local Time Conditions

	/**
	 * Represents the location where the weather record is captured.
	 */
	private String location;

	/**
	 * Coordinates of the location.
	 */
	private double latitude;
	/**
	 * Longitude of location.
	 */
	private double longitude;
	/**
	 * Elevation of location from sea level.
	 */
	private double elevation;

	/**
	 * Calendar represents the time when the weather record captured.
	 */
	private Calendar time;

	/**
	 * Indicates the summary of weather condition. Can be either of
	 * <em>Rain,Snow</em> or <em>Sunny</em>
	 */
	private Climate climateCondition;

	/**
	 * Temperature meassured.
	 */
	private double temperature;
	/**
	 * Pressure meassured.
	 */
	private double pressure;
	/**
	 * Humidity meassured.
	 */
	private double humidity;

	/**
	 * @return the location
	 */
	public String getLocation() {
		return location;
	}

	/**
	 * @param location the location to set
	 */
	public void setLocation(String location) {
		this.location = location;
	}

	/**
	 * @return the latitude
	 */
	public double getLatitude() {
		return latitude;
	}

	/**
	 * @param latitude the latitude to set
	 */
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	/**
	 * @return the longitude
	 */
	public double getLongitude() {
		return longitude;
	}

	/**
	 * @param longitude the longitude to set
	 */
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	/**
	 * @return the elevation
	 */
	public double getElevation() {
		return elevation;
	}

	/**
	 * @param elevation the elevation to set
	 */
	public void setElevation(double elevation) {
		this.elevation = elevation;
	}

	/**
	 * @return the time
	 */
	public Calendar getTime() {
		return time;
	}

	/**
	 * @param time the time to set
	 */
	public void setTime(Calendar time) {
		this.time = time;
	}

	/**
	 * @return the climateCondition
	 */
	public Climate getClimateCondition() {
		return climateCondition;
	}

	/**
	 * @param climateCondition the climateCondition to set
	 */
	public void setClimateCondition(Climate climateCondition) {
		this.climateCondition = climateCondition;
	}

	/**
	 * @return the temperature
	 */
	public double getTemperature() {
		return temperature;
	}

	/**
	 * @param temperature the temperature to set
	 */
	public void setTemperature(double temperature) {
		this.temperature = temperature;
	}

	/**
	 * @return the pressure
	 */
	public double getPressure() {
		return pressure;
	}

	/**
	 * @param pressure the pressure to set
	 */
	public void setPressure(double pressure) {
		this.pressure = pressure;
	}

	/**
	 * @return the humidity
	 */
	public double getHumidity() {
		return humidity;
	}

	/**
	 * @param humidity the humidity to set
	 */
	public void setHumidity(double humidity) {
		this.humidity = humidity;
	}

	public WeatherDTO() {
	}

}
